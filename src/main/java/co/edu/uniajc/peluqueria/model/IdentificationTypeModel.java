package co.edu.uniajc.peluqueria.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import java.sql.Timestamp;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "identification_types")
public class IdentificationTypeModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idt_id")
    private long id;

    @Column(name = "idt_name")
    private String name;

    @CreationTimestamp
    @Column(name = "idt_created_at")
    private Timestamp createdAt;

    @Column(name = "idt_state")
    private Boolean state;
}
