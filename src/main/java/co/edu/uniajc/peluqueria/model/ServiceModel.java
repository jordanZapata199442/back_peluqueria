package co.edu.uniajc.peluqueria.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="services")
public class ServiceModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "svc_id")
    private long id;

    @Column(name = "svc_name")
    private String name;

    @Column(name = "svc_description")
    private String description;

    @CreationTimestamp
    @Column(name = "svc_created_at")
    private Timestamp createdAt;

    @Column(name = "svc_state")
    private Boolean state;

}
