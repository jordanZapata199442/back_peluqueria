package co.edu.uniajc.peluqueria.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import java.sql.Timestamp;


@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "professionals")
public class ProfessionalModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "prfl_identification")
    private long identification;

    @Column(name = "prfl_name")
    private String name;

    @Column(name = "prfl_last_name")
    private String lastName;

    @Column(name = "prfl_birthday")
    private Timestamp birthday;

    @CreationTimestamp
    @Column(name = "prfl_created_at")
    private Timestamp createdAt;

    @Column(name = "prfl_state")
    private Boolean state;
}
