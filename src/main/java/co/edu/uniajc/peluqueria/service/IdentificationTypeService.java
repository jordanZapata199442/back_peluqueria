package co.edu.uniajc.peluqueria.service;

import co.edu.uniajc.peluqueria.model.IdentificationTypeModel;
import co.edu.uniajc.peluqueria.repository.IdentificationTypeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IdentificationTypeService {

    private final IdentificationTypeRepository identificationTypeRepository;

    public IdentificationTypeService(IdentificationTypeRepository identificationTypeRepository) {
        this.identificationTypeRepository = identificationTypeRepository;
    }

    public IdentificationTypeModel createIdentificationType (IdentificationTypeModel identificationTypeModel){
        return identificationTypeRepository.save(identificationTypeModel);
    }

    public IdentificationTypeModel updateIdentificationType (IdentificationTypeModel identificationTypeModel){
        return identificationTypeRepository.save(identificationTypeModel);
    }

    public void deleteIdentificationType (Long id){
        identificationTypeRepository.deleteById(id);
    }

    public List<IdentificationTypeModel> findAllIdentificationsTypes(){
        return identificationTypeRepository.findAll();
    }
}
