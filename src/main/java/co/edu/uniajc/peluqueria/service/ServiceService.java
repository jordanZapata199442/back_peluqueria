package co.edu.uniajc.peluqueria.service;

import co.edu.uniajc.peluqueria.model.ServiceModel;
import co.edu.uniajc.peluqueria.repository.ServiceRepository;
import org.springframework.stereotype.Service;

@Service
public class ServiceService {
    private final ServiceRepository serviceRepository;

    public ServiceService(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    public ServiceModel createService(ServiceModel serviceModel) {
        return serviceRepository.save(serviceModel);
    }

    public void deleteService(Long id) {
        serviceRepository.deleteById(id);
    }
}
