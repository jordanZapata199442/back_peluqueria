package co.edu.uniajc.peluqueria.repository;

import co.edu.uniajc.peluqueria.model.IdentificationTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IdentificationTypeRepository extends JpaRepository<IdentificationTypeModel, Long> {

    List<IdentificationTypeModel> findAllByState(Boolean state);

    IdentificationTypeModel getById(Long id);
}
