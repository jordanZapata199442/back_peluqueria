package co.edu.uniajc.peluqueria.repository;

import co.edu.uniajc.peluqueria.model.ServiceModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ServiceRepository extends JpaRepository<ServiceModel,Long> {

    List<ServiceModel> findAllByState(Boolean state);
}
