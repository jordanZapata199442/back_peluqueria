package co.edu.uniajc.peluqueria.controller;

import co.edu.uniajc.peluqueria.model.IdentificationTypeModel;
import co.edu.uniajc.peluqueria.service.IdentificationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/identification_types")
public class IdentificationTypeController {

    private final IdentificationTypeService identificationTypeService;

    @Autowired
    public IdentificationTypeController(IdentificationTypeService identificationTypeService) {
        this.identificationTypeService = identificationTypeService;
    }

    @PostMapping
    public IdentificationTypeModel saveStudent(@RequestBody IdentificationTypeModel identificationTypeModel) {
        return identificationTypeService.createIdentificationType(identificationTypeModel);
    }

    @PutMapping(path = "/{id}")
    public IdentificationTypeModel updateStudent(@RequestBody IdentificationTypeModel identificationTypeModel) {
        return identificationTypeService.updateIdentificationType(identificationTypeModel);
    }

    @GetMapping
    public List<IdentificationTypeModel> findAllStudent(){
        return identificationTypeService.findAllIdentificationsTypes();
    }

    @DeleteMapping(path = "/{id}")
    public void deleteStudent(@RequestParam("id") Long id) {
        identificationTypeService.deleteIdentificationType(id);
    }
}
