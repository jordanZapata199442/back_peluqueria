package co.edu.uniajc.peluqueria.controller;

import co.edu.uniajc.peluqueria.model.IdentificationTypeModel;
import co.edu.uniajc.peluqueria.model.ServiceModel;
import co.edu.uniajc.peluqueria.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/services")
public class ServiceController {

    private final ServiceService serviceService;

    @Autowired
    public ServiceController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @PostMapping
    public ServiceModel createService (@RequestBody ServiceModel serviceModel){
        return serviceService.createService(serviceModel);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteService(@PathVariable("id") Long id) {
        serviceService.deleteService(id);
    }
}
