CREATE TABLE "services"(
    svc_id SERIAL NOT NULL,
    svc_name VARCHAR(250),
    svc_description VARCHAR(250),
    svc_created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    svc_state BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY(svc_id)
);

ALTER TABLE "services" OWNER TO postgres;

CREATE TABLE "identification_types"(
    idt_id SERIAL NOT NULL,
    idt_name VARCHAR(250),
    idt_created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    idt_state BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY(idt_id)
);

ALTER TABLE "identification_types" OWNER TO postgres;

CREATE TABLE "professionals"(
    prfl_identification VARCHAR(20) NOT NULL,
    prfl_identification_type INTEGER NOT NULL,
    prfl_name VARCHAR(250),
    prfl_last_name VARCHAR(250),
    prfl_birthday TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    prfl_created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    prfl_state BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY(prfl_identification, prfl_identification_type)
);

ALTER TABLE "professionals" ADD CONSTRAINT "fk_identification_types_profesionals" FOREIGN KEY(prfl_identification_type) REFERENCES "identification_types"(idt_id);
ALTER TABLE "professionals" OWNER TO postgres;